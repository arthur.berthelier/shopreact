import './App.css';
import React, {Component} from 'react';
import { BrowserRouter, Route, Link } from "react-router-dom";
import {connect} from 'react-redux';
import Header from "./components/Header";
import Home from './views/Home';
import Textiles from './views/Articles/Textiles';
import Sacs from './views/Articles/Sacs';
import SacAdd from './views/Articles/SacAdd';
import SacInfo from './views/Articles/SacInfo';
import TextileDetails from './views/Articles/TextileDetails';
import TextileAdd from './views/Articles/TextileAdd';
import Categories from './views/Categories';
import Profil from './views/Profil';
import Panier from './views/Panier';
import Commande from './views/Commande';
import Login from './views/Login';

class App extends Component{

    render(){
        return <BrowserRouter>
            <Header/>
            {
                 this.props.user && this.props.user.role === 10 && <div>
                    <Route exact path="/categories" component={Categories} />
                    <Route exact path="/articles/sac/add" component={SacAdd} />
                    <Route exact path="/articles/textile/add" component={TextileAdd} />
                 </div>
            }
            <Route exact path="/" component={Home} />
            <Route exact path="/articles/sac" component={Sacs} />
            <Route exact path="/articles/sac/info/:id" component={SacInfo} /> 
            <Route exact path="/articles/textile" component={Textiles} />
            <Route exact path="/articles/textile/info/:id" component={TextileDetails} />
            <Route exact path="/panier" component={Panier} />
            <Route exact path="/commande" component={Commande} />
            <Route exact path="/profil" component={Profil} />
            <Route exact path="/login" component={Login} />
            
        </BrowserRouter>
    }
}


//acces au state général en pointant user
const mapStateToProps =state =>{
    return {user:state.user}
};

export default connect(mapStateToProps, null)(App);