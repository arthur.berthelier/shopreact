import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { updateUser } from "../actions/users.actions";

class Header extends Component {
    constructor(props) {
        super(props);
    }

    logout() {
        
        localStorage.removeItem('tokenShop');
        this.props.updateUser(null);
        
    }

    render() {
        return <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <a className="navbar-brand" href="/">SHOP</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div className="navbar-nav">
                    {
                        this.props.user !== null && this.props.user.role === 10 &&
                        <Link to={'/categories'} className="nav-link">Categories</Link>

                    }

                    {
                        this.props.user !== null &&
                        <div className="navbar-nav">
                            <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Artilces
                                </a>
                            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                <Link to={'/articles/sac'} className="nav-link">Sacs</Link>
                                <Link to={'/articles/textile'} className="nav-link">Textiles</Link>
                            </div>
                            <Link to={'/panier'} className="nav-link">Panier</Link>
                            <Link to={'/commande'} className="nav-link">Commande</Link>
                            <Link to={'/'} className="nav-link">Home</Link>
                            <Link to={'/profil'} className="nav-link">Profil</Link>
                        </div>
                    }

                    {/* LOGOUT */}
                    {
                        this.props.user !== null ?
                            <a className="nav-link" href="/"onClick={() => this.logout()}>Deconnexion</a>
                            : <Link to={'/login'} className="nav-link">Connexion</Link>
                    }

                </div>
            </div>

        </nav>
    }
}

const mapStateToProps = state => {
    return { user: state.user };
};

const mapDispatchToProps = dispatch => {
    return { updateUser: user => dispatch(updateUser(user)) }
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);