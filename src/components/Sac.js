import React, { Component } from 'react';
import CategoryService from '../services/category.service';

export default class Sac extends Component {

    constructor(props) {
        super(props)
        this.state = {
            categories: [],
        };
    }

    async componentDidMount() {
        try {
            let response = await CategoryService.list();
            this.setState({
                categories: response.data.categories
            });
        } catch (e) {
            console.error(e);
        }
    }

    render() {
        let { libelle, prix, image, marque, category, refermable, anse, contenance } = this.props;
        let { categories } = this.state;
        let index = category;
        return <div className="card">
            <img className="card-img-top" src={image} width={300} height={300} alt="Card image cap" />
            <div className="card-body">
                <h5 className="card-title">{libelle}</h5>
                <p className="card-text">{marque}</p>
                <p className="card-text">Refermable : {refermable}</p>
                <p className="card-text">Anse : {anse}</p>
                <p className="card-text">Contenance : {contenance}</p>
                <p className="card-text">Categorie: {categories.map((category) => {
                    if (index == category._id) {
                        return category.libelle;
                    }
                })
                }</p>
                <p className="card-text">Prix : {prix} €</p>
                <div>
                </div>
            </div>
        </div>
    }
}