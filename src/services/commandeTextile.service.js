import axios from "axios";

export default class CommandeTextileService {

    /**
     * Get panier list textile ( se ref au status de l'element dans la table commande)
     * 
     */
    static async listPanier(id) {
        return await axios.get(`${process.env.REACT_APP_HOST_API}/articles/textile/listPanier/${id}`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenShop')}`
            }
        });
    }

    /**
     * Get commande textile list ( se ref au status de l'element dans la table commande)
     * 
     */
    static async listCommande(id) {
        return await axios.get(`${process.env.REACT_APP_HOST_API}/articles/textile/listCommande/${id}`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenShop')}`
            }
        });
    }

    /**
     * Add one textile to the panier
     */
    static async add(body) {
        return await axios.post(`${process.env.REACT_APP_HOST_API}/articles/textile/add`, body, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenShop')}`
            }
        });
    }

     /**
     * Add one textile from the panier to the commandes
     */
    static async update(body,id) {
        return await axios.put(`${process.env.REACT_APP_HOST_API}/articles/textile/update/${id}`, body, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenShop')}`
            }
        });
    }

    /**
     * Add all textile from the panier to the commandes
     */
    static async updateAll(id) {
        return await axios.put(`${process.env.REACT_APP_HOST_API}/articles/textile/updateAll/${id}`,null, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenShop')}`
            }
        });
    }


    /**
    * delete one textile of panier
    */
    static async deletePanier(id,id_user) {
        return await axios.delete(`${process.env.REACT_APP_HOST_API}/articles/textile/deletePanier/${id}/${id_user}`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenShop')}`
            }
        });
    }
}