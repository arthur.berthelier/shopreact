import axios from "axios";

export default class CategoryService {

    /**
     * Get categories's list
     * 
     */
    static async list() {
        return await axios.get(`${process.env.REACT_APP_HOST_API}/categories`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenShop')}`
            }
        });
    }

    /**
     * Create one category
     */
    static async create(body) {
        return await axios.post(`${process.env.REACT_APP_HOST_API}/categories`, body, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenShop')}`
            }
        });
    }

    /**
     * update one category
     */
    static async update(id, body) {
        return await axios.put(`${process.env.REACT_APP_HOST_API}/categories/${id}`, body,  {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenShop')}`
            }
        });
    }

    
    /**
    * delete one category
    */
    static async delete(id) {
        return await axios.delete(`${process.env.REACT_APP_HOST_API}/categories/${id}`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenShop')}`
            }
        });
    }
}