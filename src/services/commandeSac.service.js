import axios from "axios";

export default class CommandeSacService {

    /**
     * Get panier sac list ( se ref au status de l'element dans la table commande)
     * 
     */
    static async listPanier(id) {
        return await axios.get(`${process.env.REACT_APP_HOST_API}/articles/sac/listPanier/${id}`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenShop')}`
            }
        });
    }

    /**
     * Get commande sac list ( se ref au status de l'element dans la table commande)
     * 
     */
    static async listCommande(id) {
        return await axios.get(`${process.env.REACT_APP_HOST_API}/articles/sac/listCommande/${id}`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenShop')}`
            }
        });
    }

    /**
     * Add one sac to the commande or to the panier depending of the status 
     */
    static async add(body) {
        return await axios.post(`${process.env.REACT_APP_HOST_API}/articles/sac/add`, body, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenShop')}`
            }
        });
    }

    /**
     * Add one sac from the panier to the commandes
     */
    static async update(body,id) {
        console.log(body);
        console.log(id);
        return await axios.put(`${process.env.REACT_APP_HOST_API}/articles/sac/update/${id}`, body, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenShop')}`
            }
        });
    }

    /**
     * Add all sac from the panier to the commandes
     */
    static async updateAll(id) {
        return await axios.put(`${process.env.REACT_APP_HOST_API}/articles/sac/updateAll/${id}`, null,{
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenShop')}`
            }
        });
    }

    /**
    * delete one sac of panier
    */
    static async deletePanier(id,id_user) {
        return await axios.delete(`${process.env.REACT_APP_HOST_API}/articles/sac/deletePanier/${id}/${id_user}`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenShop')}`
            }
        });
    }
}