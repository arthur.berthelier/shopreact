import axios from "axios";

export default class UserService{
    
    //auth user and return token 
    static async auth(body){
        return await axios.post(`${process.env.REACT_APP_HOST_API}/users/auth`, body);
    }

    static isAuth(){
        return localStorage.getItem('tokenShop') !== null;
    }

    /**
     * Create an user
     */
    static async create(body) {
        return await axios.post(`${process.env.REACT_APP_HOST_API}/users/`, body, {
        });
    }

    /**
     * update an user
     */
    static async details(id) {
        return await axios.get(`${process.env.REACT_APP_HOST_API}/users/${id}`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenShop')}`
            }
        });
    }

    /**
     * update an user
     */
    static async update(id, body) {
        return await axios.put(`${process.env.REACT_APP_HOST_API}/users/${id}`,body, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenShop')}`
            }
        });
    }

    /**
    * delete an user
    */
   static async delete(id) {
    return await axios.delete(`${process.env.REACT_APP_HOST_API}/users/${id}`, {
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('tokenShop')}`
        }
    });
}
}