import axios from "axios";

export default class SacService {

    /**
     * Get sac's list
     * 
     */
    static async list() {
        return await axios.get(`${process.env.REACT_APP_HOST_API}/articles/sac`,{
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenShop')}`
            }
        });
    }

    /**
     * Create one sacs
     */
    static async create(body) {
        return await axios.post(`${process.env.REACT_APP_HOST_API}/articles/sac`, body,  {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenShop')}`
            }
        });
    }

    /**
     * update one sacs
     */
    static async update(id, body) {
        return await axios.put(`${process.env.REACT_APP_HOST_API}/articles/sac/${id}`, body,  {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenShop')}`
            }
        });
    }

    /**
   * update one image sacs
   */
    static async updateImage(id, body) {
        return await axios.put(`${process.env.REACT_APP_HOST_API}/articles/sac/${id}/image`, body, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenShop')}`
            }
        });
    }

    /**
    * delete one sacs
    */
    static async delete(id) {
        return await axios.delete(`${process.env.REACT_APP_HOST_API}/articles/sac/${id}`,{
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenShop')}`
            }
        });
    }
}