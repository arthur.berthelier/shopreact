import axios from "axios";

export default class TextileService {

    /**
     * Get textile's list
     * 
     */
    static async list() {
        return await axios.get(`${process.env.REACT_APP_HOST_API}/articles/textile`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenShop')}`
            }
        });
    }

    /**
     * Create one textile
     */
    static async create(body) {
        return await axios.post(`${process.env.REACT_APP_HOST_API}/articles/textile`, body,  {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenShop')}`
            }
        });
    }
   
    /**
     * update one textile
     */
    static async update(id, body) {
        
        return await axios.put(`${process.env.REACT_APP_HOST_API}/articles/textile/${id}`,body,  {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenShop')}`
            }
        });
    }

       /**
     * update one image textile
     */
    static async updateImage(id, body) {
        return await axios.put(`${process.env.REACT_APP_HOST_API}/articles/textile/${id}/image`,body, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenShop')}`
            }
        });
    }

    /**
    * delete one textile
    */
    static async delete(id) {
        return await axios.delete(`${process.env.REACT_APP_HOST_API}/articles/textile/${id}`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenShop')}`
            }
        });
    }
}