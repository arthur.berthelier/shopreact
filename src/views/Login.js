import React, { Component } from 'react';
import UserService from '../services/user.service';
import { connect } from 'react-redux';
import { updateUser } from '../actions/users.actions';

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            mail: null,
            password: null,
            isError: false,
            firstname: null,
            lastname: null,
            mail: null,
            password: null,
            phone: null,
            address: null,
            zip_code: null,
            open: false,
            openConnexion: true, 
        }
        this.togglebutton = this.togglebutton.bind(this);
    }

    handleChange(e) {
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    async submit(e) {
        e.preventDefault();
        this.setState({ isError: false })
        let { mail, password } = this.state;
        let body = { mail, password };
        try {
            let response = await UserService.auth(body);
            let token = response.data.token;
            localStorage.setItem('tokenShop', token);
            this.props.updateUser(response.data.user);
            this.props.history.push('/');
        } catch (e) {
            this.setState({ isError: true })
        }
    }

  
    async submitCreate(e) {
        e.preventDefault();

        this.setState({ isError: false })
        let { firstname, lastname, mail, password, phone, address, zip_code } = this.state;

        let body = { firstname: firstname, lastname: lastname, mail: mail, password: password, phone: phone, address: address, zip_code: zip_code };
        let body2 = { mail, password };
        try {
            await UserService.create(body);

            let response = await UserService.auth(body2);
            let token = response.data.token;
            localStorage.setItem('tokenShop', token);
            this.props.updateUser(response.data.user);
            this.props.history.push('/');
        } catch (e) {
            this.setState({ isError: true })
        }
    }

    togglebutton() {
        let { open, openConnexion } = this.state;  
        this.setState({  
            open: !open, 
            openConnexion: !openConnexion 
        });    
    }

    render() {
        let { isError, open, openConnexion } = this.state;
        return <div className="container">
            {/* connexion */}
            
            {openConnexion === true ? <div className="card hidden" >
                <div className="row">
                    <div className="col">
                        <div className="card-block px-4">
                            <div className="col-lg">
                                <div className="p-4">
                                    <div className="text-center">
                                        <h1>Connectez vous !</h1>
                                    </div>

                                    <form onSubmit={(e) => this.submit(e)} >
                                        <div className="form-group">
                                            <label>Email</label>
                                            <input type="mail" required className="form-control" id="mail"
                                                onChange={(e) => this.handleChange(e)} />
                                        </div>
                                        <div className="form-group">
                                            <label>Mot de passe</label>
                                            <input type="password" required className="form-control" id="password"
                                                onChange={(e) => this.handleChange(e)} />
                                        </div>
                                        <button type="submit" className='btn btn-primary'>Connexion</button>
                                        {
                                            isError &&
                                            <p>Erreur email ou mdp inconnu</p>
                                        }

                                        <p>{this.props.name}</p>
                                    </form>

                                    <button className="btn btn-success" onClick={this.togglebutton}>
                                        Vous n'avez pas de compte ? Inscrivez-vous !
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-auto">
                        <img src="/img/logo_connexion.png"></img>
                    </div>
                </div>
            </div>
            : null }


            {open === true ? <div className="card hidden" >
                <div className="row">
                    <div className="col">
                        <div className="card-block px-4">
                            <div className="col-lg">
                                <div className="p-4">
                                    <div className="text-center">
                                        <h1>Inscrivez-vous !</h1>
                                    </div>
                                    <form onSubmit={(e) => this.submitCreate(e)} >
                                        <div className="form-group">
                                            <label>Prénom</label>
                                            <input type="string" required className="form-control" id="firstname"
                                                onChange={(e) => this.handleChange(e)} />
                                        </div>
                                        <div className="form-group">
                                            <label>Nom</label>
                                            <input type="string" required className="form-control" id="lastname"
                                                onChange={(e) => this.handleChange(e)} />
                                        </div>
                                        <div className="form-group">
                                            <label>Email</label>
                                            <input type="mail" required className="form-control" id="mail"
                                                onChange={(e) => this.handleChange(e)} />
                                        </div>
                                        <div className="form-group">
                                            <label>Mot de passe</label>
                                            <input type="password" required className="form-control" id="password"
                                                onChange={(e) => this.handleChange(e)} />
                                        </div>
                                        <div className="form-group">
                                            <label>Téléphone</label>
                                            <input type="number" className="form-control" id="phone"
                                                onChange={(e) => this.handleChange(e)} />
                                        </div>
                                        <div className="form-group">
                                            <label>Adresse</label>
                                            <input type="string" className="form-control" id="address"
                                                onChange={(e) => this.handleChange(e)} />
                                        </div>
                                        <div className="form-group">
                                            <label>Code postal</label>
                                            <input type="string" required className="form-control" id="zip_code"
                                                onChange={(e) => this.handleChange(e)} />
                                        </div>
                                        <button type="submit" className='btn btn-primary'>Inscription</button>
                                    </form>
                                    <button className="btn btn-success" onClick={this.togglebutton}>
                                        Vous avez déjà un compte ? Connectez-vous !
                                </button>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            : null }
        </div>
    }
}

//UTILISATION DE REDUX POUR L'IDENTIFICATION
// LECTURE RETURN un object avec des attributs du state globale permet de renvoyer le user
const mapStateToProps = state => {
    return {user: state.user}
};

//ECRITURE RETURN un object avec des fonctiion utiliser pour modifier une valeur affecter une valeur 
const mapDispatchToProps = dispatch => {
    return {
        updateUser: user=> dispatch(updateUser(user))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);