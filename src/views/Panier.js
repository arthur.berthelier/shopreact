import React, { Component } from 'react';
import SacCommandeService from '../services/commandeSac.service';
import SacService from '../services/sac.service';
import Sac from "../components/Sac";
import TextileCommandeService from '../services/commandeTextile.service';
import Textile from "../components/Textile";
import { connect } from 'react-redux';

class Panier extends Component {

    constructor(props) {
        super(props);
        this.state = {
            articles: [],
            textiles:[]
        }
    }

    async componentDidMount(){
        try {
            let response_sac = await SacCommandeService.listPanier(this.props.user._id); 
            let response_textile = await TextileCommandeService.listPanier(this.props.user._id); 
            this.setState({articles: response_sac.data.articles})
            this.setState({textiles: response_textile.data.articles})
        } catch (e) {
            console.error(e);
        }
    }

    async submitSac(e, id_article) {
        e.preventDefault();
        try {
            let body = {id_article:id_article}
            await SacCommandeService.update(body,this.props.user._id);
            this.props.history.push('/commande');
        } catch (e) {
            console.error(e);
        }
    }

    async deleteSac(e,id_article){
        e.preventDefault();
        try {
            await SacCommandeService.deletePanier(id_article,this.props.user._id);
            let response_sac = await SacCommandeService.listPanier(this.props.user._id);  
            this.setState({articles: response_sac.data.articles})
        } catch (e) {
            console.error(e);
        }
    }

    async submitTextile(e, id_article) {
        e.preventDefault();
        try {
            let body = {id_article:id_article}
            await TextileCommandeService.update(body,this.props.user._id);
            this.props.history.push('/commande');
        } catch (e) {
            console.error(e);
        }
    }

    async deleteTextile(e,id_article){
        e.preventDefault();
        try {
            await TextileCommandeService.deletePanier(id_article,this.props.user._id);
            let response_textile = await TextileCommandeService.listPanier(this.props.user._id); 
            this.setState({textiles: response_textile.data.articles})
        } catch (e) {
            console.error(e);
        }
    }

    
    async submitPanier(e){
        e.preventDefault();
        try {
            await TextileCommandeService.updateAll(this.props.user._id);
            await SacCommandeService.updateAll(this.props.user._id);
            this.props.history.push('/commande');
        } catch (e) {
            console.error(e);
        }
    }
   

    render() {
        let { articles, textiles } = this.state;
        return <div className="container-fluid">

            <h1>Votre panier</h1>
            {
                <form  onSubmit={(e) => this.submitPanier(e)}>
                    <button type="submit" className="btn btn-success">Commander le panier</button>
                </form>
            }
            <h2>Vos Sacs</h2>
            <div className="row">
                {
                    articles.map((article, index_sac) => {
                        return <div className="col-md-2 mb-3">
                            <Sac
                            key={index_sac}
                            libelle={article.libelle}
                            marque={article.marque}
                            prix={article.prix}
                            stock={article.stocke}
                            image={`${process.env.REACT_APP_HOST_API}/${article.image}`}
                            refermable={article.refermable === true ? 'Oui' : 'Non'}
                            anse={article.anse === true ? 'Oui' : 'Non'}
                            contenance={article.contenance}
                            category={article.category}
                        />
                        {
                            this.props.user !== null &&
                            <form onSubmit={(e) => this.submitSac(e,article.id_article)}>
                                <button type="submit" className="btn btn-success">Ajouter aux commandes</button>
                            </form>
                        }
                        {
                            <form  onSubmit={(e) => this.deleteSac(e,article.id_article)}>
                                <button type="submit" className="btn btn-danger">Supprimer</button>
                            </form>
                        }
                     </div>
                       
                    })  
                }
            </div> 

            <h2>Vos Textiles</h2>
        
            <div className="row">
                {
                    textiles.map((textile, index_textile) => {
                        return <div className="col-md-2 mb-3">
                            <Textile
                            key={index_textile}
                            libelle={textile.libelle}
                            marque={textile.marque}
                            prix={textile.prix}
                            stock={textile.stocke}
                            image={`${process.env.REACT_APP_HOST_API}/${textile.image}`}
                            taille={textile.taille}
                            elasthanne={textile.elasthanne === true ? 'Oui' : 'Non'}
                            lavage={textile.lavage}
                            category={textile.category}
                        />
                          {
                            this.props.user !== null &&
                            <form onSubmit={(e) => this.submitTextile(e,textile.id_article)}>
                                <button type="submit" className="btn btn-success">Ajouter aux commandes</button>
                            </form>
                        }
                        {
                            <form  onSubmit={(e) => this.deleteTextile(e,textile.id_article)}>
                                <button type="submit" className="btn btn-danger">Supprimer</button>
                            </form>
                        }
                        </div>
                    })
                }
            </div> 
        </div>
    }
}
//UTILISATION DE REDUX POUR L'IDENTIFICATION
// LECTURE RETURN un object avec des attributs du state globale permet de renvoyer le user
const mapStateToProps = state => {
    return {user: state.user}
};



export default connect(mapStateToProps, null)(Panier);