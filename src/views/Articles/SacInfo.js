import React, { Component } from 'react'
import SacService from '../../services/sac.service';
import CategoryService from '../../services/category.service';
import { connect } from 'react-redux';
import SacCommandeService from '../../services/commandeSac.service';
class SacInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sacs: [],
            sac: [],
            libelle: null,
            marque: null,
            image: null,
            prix: null,
            stock: null,
            contenance: null,
            anse: null,
            refermable: null,
            updatedSac: null,
            open: false,
            category: null,
            categories: [],
            index:null,
            sacImage:null
        }
        this.togglebutton = this.togglebutton.bind(this);
        this.backButton = this.backButton.bind(this); //affectation entre onclick et le bouton 
    }

    togglebutton() {
        let { open } = this.state;
        this.setState({
            open: !open,
        });
    }

    backButton(){
        try{
            this.props.history.goBack()
        } catch (e) {
            console.error(e);
        }  
    }

    async componentDidMount() {
        try {
            let { id } = this.props.match.params;
            let response = await SacService.list();
            let response2 = await CategoryService.list();
            let sacs = response.data.articles;
            let sac = sacs.find(item => item._id === id);
            let categories = response2.data.categories;
            let index = sac.category;
            let category = null;
            let cat_libelle = null;
            categories.forEach(c => {
                if(index == c._id) {
                    category = c._id
                    cat_libelle= c.libelle
                }
            })
            this.setState({
                sac: sac,
                libelle: sac.libelle,
                marque: sac.marque,
                image: `${process.env.REACT_APP_HOST_API}/${sac.image}`,
                sacImage: sac.image,
                prix: sac.prix,
                stock: sac.stock,
                contenance: sac.contenance,
                anse: sac.anse,
                refermable: sac.refermable,
                index:index,
                category: category,
                cat_libelle: cat_libelle,
                categories: categories
            });
          
        } catch (e) {
            console.error(e);
        }
    }

    handleChange(e) {
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    handleChangeImage(e) {
        this.setState({
            image: e.target.files[0],
        })
    }

    async submit(e) {
        e.preventDefault();
        try {
            let { id } = this.props.match.params;
            let { libelle, marque, prix, stock, category, contenance, anse, refermable } = this.state;

            this.setState({ 
                libelle: libelle, 
                marque: marque, 
                prix: prix, 
                stock: stock, 
                contenance: contenance, 
                anse: anse, 
                refermable: refermable, 
                category: category });

            let body = { 
                libelle: libelle, 
                marque: marque, 
                prix: prix, 
                stock: stock, 
                contenance: contenance, 
                anse: anse, 
                refermable: refermable, 
                category: category 
            }

            await SacService.update(id, body);
            this.props.history.push(`/articles/sac`);
        } catch (e) {
            console.error(e);
        }
    }

    async submitImage(e) {
        e.preventDefault();
        let { id } = this.props.match.params;
        let { image } = this.state;

        let formData2 = new FormData();
        formData2.append('image', image);

        try {
            await SacService.updateImage(id, formData2);
            this.props.history.push('/articles/sac');
        } catch (e) {
            console.error(e);
        }
    }
    async submitSac(e, id_article, category,article_image,libelle, marque, prix, contenance, anse, refermable) {
        e.preventDefault();

        let body = {libelle: libelle, image: article_image, marque: marque, prix: prix, contenance: contenance, anse: anse, refermable: refermable,id_article:id_article,status:"en cours",id_user:this.props.user._id,category:category}
        try {
            await SacCommandeService.add(body);
            this.props.history.push('/panier');
        } catch (e) {
            console.error(e);
        }
    }

    render() {
        let { sac, index,libelle, prix, image, marque, stock,cat_libelle, categories, refermable, anse, contenance, open,category,sacImage } = this.state;
        let { id } = this.props.match.params;
        return <div className="container">
            <div className="d-grid gap-2 d-md-flex justify-content-md-end">
                <button type="onClick" className="btn btn-primary" onClick={this.backButton}>Retour</button>
            </div>
            <div className="card">
            
                <img className="card-img" src={image} width={200} height={500} alt="Card image cap" />
                <div className="card-body">
                    <h1 className="card-title">Détail du sac : {sac.libelle}</h1>
                    <h5 className="card-text">Marque : {sac.marque}</h5>
                    <h5 className="card-text">Refermable : {sac.refermable === true ? 'Oui' : 'Non'}</h5>
                    <h5 className="card-text">Anse : {sac.anse === true ? 'Oui' : 'Non'}</h5>
                    <h5 className="card-text">Contenance : {sac.contenance}</h5>
                    <h5 className="card-text">Categorie: {cat_libelle}</h5>
                    <h5 className="card-text">Prix : {sac.prix} €</h5>
                </div>
               
                {
                    this.props.user !== null &&
                    <form encType="multipart/form-data" onSubmit={(e) => this.submitSac(e,id,index,sacImage,libelle,marque,prix,contenance,anse,refermable)}>
                        <button type="submit" className="btn btn-success">Ajouter au panier</button>
                    </form>
                  
                }
                {
                    this.props.user.role === 10 &&
                    <button type="onClick" className="btn btn-warning" onClick={this.togglebutton}>
                        Modifier
                    </button>
                }

            </div>

            <hr />
            {open === true ? <div className="card hidden" >
                <h2> Modification SAC</h2>
                <form encType="multipart/form-data" onSubmit={(e) => this.submit(e)} >

                    <div className="row">
                        <div className="col-md-4">
                            <div className="form-group">
                                <label> Libelle </label>
                                <input type="text" id="libelle" className="form-control" value={libelle} required onChange={(e) => this.handleChange(e)} />
                            </div>
                        </div>

                        <div className="col-md-4">
                            <div className="form-group">
                                <label> Marque </label>
                                <input type="text" id="marque" className="form-control" value={marque} required onChange={(e) => this.handleChange(e)} />
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-2">
                            <div className="form-group">
                                <label> Prix </label>
                                <input type="number" id="prix" className="form-control" value={prix} required onChange={(e) => this.handleChange(e)} />
                            </div>
                        </div>

                        <div className="col-md-2">
                            <div className="form-group">
                                <label> Stock </label>
                                <input type="number" id="stock" className="form-control" value={stock} onChange={(e) => this.handleChange(e)} />
                            </div>
                        </div>

                        <div className="col-md-2">
                            <div className="form-group">
                            <label> Categorie </label>
                                <select id="category"  required className="form-control" onChange={(e) => this.handleChange(e)}>
                                        {
                                            categories.map((category, toto) => {
                                                if(category._id == index){
                                                    return <option key={toto} selected value={category._id}> {category.libelle} </option>
                                                }
                                                else{
                                                } return <option key={toto} value={category._id}> {category.libelle} </option>
                                            })
                                        }
                                </select>
                            </div>
                        </div>

                        <div className="col-md-2">
                            <div className="form-group">
                                <label> Contenance </label>
                                <input type="number" id="contenance" className="form-control" value={contenance} required onChange={(e) => this.handleChange(e)} />
                            </div>
                        </div>

                        <div className="col-md-2">
                            <div className="form-group">
                                <label> Anse </label>
                                <select type="boolean" id="anse" className="form-control" value={anse} required onChange={(e) => this.handleChange(e)}>
                                    <option value={true}> Oui </option>
                                    <option value={false}> Non </option>
                                </select>
                            </div>
                        </div>

                        <div className="col-md-2">
                            <div className="form-group">
                                <label> Refermable </label>
                                <select id="refermable" className="form-control" value={refermable} required onChange={(e) => this.handleChange(e)}>
                                    <option value={true}> Oui </option>
                                    <option value={false}> Non </option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <br></br>
                    <button type="submit" className="btn btn-success">
                        Valider
                    </button>
                </form>
            </div>
                : null
            }

            {open === true ? <div className="card hidden" >
                <h2> Modification Image</h2>
                <form encType="multipart/form-data" onSubmit={(e) => this.submitImage(e)} >



                    <div className="row">
                        <div className="col-md-4">
                            <div className="form-group">
                                <label> Image </label>
                                <input type="file" id="image" className="form-control" required onChange={(e) => this.handleChangeImage(e)} />
                            </div>
                        </div>
                    </div>

                    <br></br>
                    <button type="submit" className="btn btn-success">
                        Valider Image
                    </button>
                </form>
            </div>
                : null
            }

        </div>
    }
}

//acces au state général en pointant user
const mapStateToProps = state => {
    return { user: state.user }
};

export default connect(mapStateToProps, null)(SacInfo);