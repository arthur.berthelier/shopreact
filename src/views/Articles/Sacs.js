import React, { Component } from 'react'
import SacService from '../../services/sac.service';
import Sac from '../../components/Sac';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import SacCommandeService from '../../services/commandeSac.service';


class Sacs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sacs: [],
        }
        this.backButton = this.backButton.bind(this);
    }

    backButton() {
        try {
            this.props.history.goBack()
        } catch (e) {
            console.error(e);
        }
    }

    async componentDidMount() {

        try {
            let response = await SacService.list();
            this.setState({ sacs: response.data.articles });
        } catch (e) {
            console.error(e);
        }
    }
    async submitSac(e, id_article, category,article_image,libelle, marque, prix, contenance, anse, refermable) {
        e.preventDefault();

        let body = {libelle: libelle, image: article_image, marque: marque, prix: prix, contenance: contenance, anse: anse, refermable: refermable,id_article:id_article,status:"en cours",id_user:this.props.user._id,category:category}
        try {
            await SacCommandeService.add(body);
            this.props.history.push('/panier');
        } catch (e) {
            console.error(e);
        }
    }

  
    async deleteSac(id) {
        try {
            await SacService.delete(id);

            let response = await SacService.list();
            this.setState({ sacs: response.data.articles });
            this.props.history.push('/articles/sac');

        } catch (e) {
            console.error(e);
        }
    }

    render() {
        let { sacs } = this.state;

        return <div className="container-fluid">

            <h1>SACS</h1>
            <div className="btn-toolbar justify-content-between" role="toolbar" aria-label="Toolbar with button groups">
                <div className="btn-group" role="group" aria-label="First group">
                    <h2>Liste des sacs </h2>
                    {
                        this.props.user.role === 10 &&
                        <Link to={'/articles/sac/add'} className="btn btn-primary">Ajouter un sac</Link>
                    }
                </div>
                <div className="input-group">
                    <button type="onClick" className="btn btn-primary" onClick={this.backButton}>Retour</button>
                </div>
            </div>
        
            {
                this.props.user !== null &&
                <div className="row">
                    {
                        sacs.map((article, index) => {
                            return <div className="card col-md-3 mb-4">
                                <Sac
                                    key={index}
                                    libelle={article.libelle}
                                    marque={article.marque}
                                    prix={article.prix}
                                    stock={article.stocke}
                                    image={`${process.env.REACT_APP_HOST_API}/${article.image}`}
                                    refermable={
                                        article.refermable === true ? 'Oui' : 'Non'}
                                    anse={article.anse === true ? 'Oui' : 'Non'}
                                    contenance={article.contenance}
                                    category={article.category}
                                />
                                <Link to={`/articles/sac/info/${article._id}`} className="btn btn-primary">Voir le sac</Link>
                                {
                                    this.props.user !== null &&
                                    <form encType="multipart/form-data" onSubmit={(e) => this.submitSac(e,article._id,article.category,article.image,article.libelle,article.marque,article.prix,article.contenance,article.anse,article.refermable)}>
                                        <button type="submit" className="btn btn-success">Ajouter au panier</button>
                                    </form>
                                }
                                {
                                    this.props.user.role === 10 &&
                                    <button className='btn btn-danger' onClick={() => this.deleteSac(article._id)}>
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-trash" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                            <path fillRule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                                            </svg>
                                    </button>
                                }
                            </div>
                        })
                    }
                </div>
            }
        </div>
    }
}

//acces au state général en pointant user
const mapStateToProps = state => {
    return { user: state.user }
};

export default connect(mapStateToProps, null)(Sacs);