import React, { Component } from 'react'
import TextileService from '../../services/textile.service';
import CategoryService from '../../services/category.service';

export default class TextileAdd extends Component {
    constructor(props) {
        super(props);
        this.state = {
            libelle: null,
            marque: null,
            image: null,
            prix: null,
            stock: null,
            taille: 'XS',
            elasthanne: true,
            lavage: null,
            activatedCategory:null,
            categories: [],
        }
        this.backButton = this.backButton.bind(this);
    }

    backButton() {
        try {
            this.props.history.goBack()
        } catch (e) {
            console.error(e);
        }
    }

    async componentDidMount() {

        try {
            let response = await CategoryService.list();
            this.setState({ 
                categories: response.data.categories  });
        } catch (e) {
            console.error(e);
        }
    }

    handleChange(e) {
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    handleChangeImage(e) {
        this.setState({
            image: e.target.files[0],
        })
    }

    async submit(e) {
        e.preventDefault();
        let { libelle, marque, image, prix, stock,activatedCategory, taille, elasthanne, lavage } = this.state;

        let formData = new FormData();
        formData.append('libelle', libelle);
        formData.append('marque', marque);
        formData.append('image', image);
        formData.append('prix', prix);
        formData.append('stock', stock);
        formData.append('category', [activatedCategory]);
        formData.append('taille', taille);
        formData.append('elasthanne', elasthanne);
        formData.append('lavage', lavage);

        try {
            await TextileService.create(formData);
            this.props.history.push('/articles/textile');
        } catch (e) {
            console.error(e);
        }
    }

    render() {
        let { categories } = this.state;
        return <div className="container-fluid">
            <div className="btn-toolbar justify-content-between" role="toolbar" aria-label="Toolbar with button groups">
                <div className="btn-group" role="group" aria-label="First group">
                <h1>Ajout d'un textile</h1>
                </div>
                <div className="input-group">
                <button  type="onClick" className="btn btn-primary" onClick={this.backButton}>Retour</button>
                </div>
            </div>
            <form encType="multipart/form-data" onSubmit={(e) => this.submit(e)} >

                <div className="row">
                    <div className="col-md-4">
                        <div className="form-group">
                            <label> Libelle </label>
                            <input type="text" id="libelle" className="form-control" required onChange={(e) => this.handleChange(e)} />
                        </div>
                    </div>

                    <div className="col-md-4">
                        <div className="form-group">
                            <label> Marque </label>
                            <input type="text" id="marque" className="form-control" required onChange={(e) => this.handleChange(e)} />
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-2">
                        <div className="form-group">
                            <label> Prix </label>
                            <input type="number" id="prix" className="form-control" required onChange={(e) => this.handleChange(e)} />
                        </div>
                    </div>

                    <div className="col-md-2">
                        <div className="form-group">
                            <label> Stock </label>
                            <input type="number" id="stock" className="form-control" required onChange={(e) => this.handleChange(e)} />
                        </div>
                    </div>

                    <div className="col-md-2">
                        <div className="form-group">
                            <label> Categorie </label>
                            <select id="activatedCategory" required className="form-control" onChange={(e) => this.handleChange(e)}>
                                <option value={''}> Choisie ta valeur</option>
                                {
                                    categories.map((category, index) => {
                                        return <option key={index} value={category._id}> {category.libelle} </option>
                                    })
                                }
                            </select>
                        </div>
                    </div>

                    <div className="col-md-2">
                        <div className="form-group">
                            <label> Taille </label>
                            <select type="boolean" id="stock"  className="form-control" required onChange={(e) => this.handleChange(e)}>
                                <option value="XS"> XS </option>
                                <option value="S"> S </option>
                                <option value="M"> M </option>
                                <option value="L"> L </option>
                                <option value="XL"> XL </option>
                            </select>
                        </div>
                    </div>

                    <div className="col-md-2">
                        <div className="form-group">
                            <label> Elasthanne </label>
                            <select type="boolean" id="elasthanne"  className="form-control" required onChange={(e) => this.handleChange(e)}>
                                <option value={true}> Oui </option>
                                <option value={false}> Non </option>
                            </select>
                        </div>
                    </div>

                    <div className="col-md-2">
                        <div className="form-group">
                            <label> Lavage </label>
                            <input type="sting" id="lavage" className="form-control" onChange={(e) => this.handleChange(e)} />
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-4">
                        <div className="form-group">
                            <label> Image </label>
                            <input type="file" id="image" className="form-control" required onChange={(e) => this.handleChangeImage(e)} />
                        </div>
                    </div>
                </div>

                <br></br>
                <button type="submit" className="btn btn-primary">
                    Ajouter
                </button>
            </form>
        </div>
    }
}

