import React, { Component } from 'react'
import TextileService from '../../services/textile.service';
import CategoryService from '../../services/category.service';
import { connect } from 'react-redux';
import TextileCommandeService from '../../services/commandeTextile.service';

class TextileDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            textiles: [],
            textile: [],
            libelle: null,
            marque: null,
            image: null,
            prix: null,
            stock: null,
            taille: null,
            lavage: null,
            elasthanne: null,
            updatedTextile: null,
            open: false,
            category: null,
            cat_libelle: null,
            categories: [],
            index:null,
            textileImage:null,
            text : null
        }
        this.togglebutton = this.togglebutton.bind(this);
        this.backButton = this.backButton.bind(this); 
    }

    togglebutton() {
        const { open } = this.state;
        this.setState({
            open: !open,
        });
    }

    backButton(){
        try{
            this.props.history.goBack()
        } catch (e) {
            console.error(e);
        }  
    }

    async componentDidMount() {
        try {
            let { id } = this.props.match.params;
            let response = await TextileService.list();
            let response2 = await CategoryService.list();
            let textiles = response.data.articles;
            let textile = textiles.find(item => item._id === id);
            let categories = response2.data.categories;
            let index = textile.category;
            let category = null;
            let cat_libelle = null;
            categories.forEach(c => {
                if(index == c._id) {
                    category = c._id
                    cat_libelle= c.libelle
                }
            })
            this.setState({
                textile: textile,
                libelle: textile.libelle,
                marque: textile.marque,
                image: `${process.env.REACT_APP_HOST_API}/${textile.image}`,
                textileImage: textile.image,
                prix: textile.prix,
                stock: textile.stock,
                index:index,
                taille: textile.taille,
                lavage: textile.lavage,
                elasthanne: textile.elasthanne,
                category: category,
                cat_libelle: cat_libelle,
                categories: categories
            });
           
        } catch (e) {
            console.error(e);
        }
    }

    handleChange(e) {
        this.setState({
            [e.target.id]: e.target.value,
           
        });
    }

    handleChangeImage(e) {
        this.setState({
            image: e.target.files[0],
        })
    }

    async submit(e) {
        e.preventDefault();
        try {
            let { id } = this.props.match.params;
            let { libelle, marque, prix, stock,category,index, taille, lavage, elasthanne } = this.state;

            this.setState({ 
                libelle: libelle, 
                marque: marque, 
                prix: prix, 
                stock: stock, 
                taille: taille, 
                lavage: lavage, 
                elasthanne: elasthanne, 
                category: category,
                
            });

            let body = { 
                libelle: libelle, 
                marque: marque, 
                prix: prix, 
                stock: stock, 
                taille: taille, 
                lavage: lavage, 
                elasthanne: elasthanne, 
                category: category,
                
            }
            
            await TextileService.update(id, body);
            this.props.history.push(`/articles/textile`);
        } catch (e) {
            console.error(e);
        }
    }
    async submitTextile(e,id_article, category,article_image,libelle, marque, prix, taille, elasthanne, lavage) {
        e.preventDefault();
        let body = {
            libelle: libelle, 
            image: article_image, 
            marque: marque, 
            prix: prix, 
            taille: taille, 
            elasthanne: elasthanne, 
            lavage: lavage,
            id_article:id_article,
            status:"en cours",
            id_user:this.props.user._id,
            category:category
        }
        console.log(body);
        try {
            await TextileCommandeService.add(body);
           
            this.props.history.push('/panier');
        } catch (e) {
            console.error(e);
        }
    }

    async submitImage(e) {
        e.preventDefault();
        let { id } = this.props.match.params;
        let { image } = this.state;

        let formData2 = new FormData();
        formData2.append('image', image);

        try {
            await TextileService.updateImage(id, formData2);
            this.props.history.push('/articles/textile');
        } catch (e) {
            console.error(e);
        }
    }

    render() {
        let { textile, libelle, marque, prix, stock, taille, lavage, categories, cat_libelle, elasthanne, open, image, index,textileImage } = this.state;
        let { id } = this.props.match.params;
        return <div className="container">
            <div className="card">
            <button type="onClick" className="btn btn-primary" onClick={this.backButton}>Retour</button>
                <img className="card-img" src={image} width={200} height={500} alt="Card image cap" />
                <div className="card-body">
                    <h1>Détail du textile : {textile.libelle}</h1>
                    <h5 className="card-text">Marque : {textile.marque}</h5>
                    <h5 className="card-text">Taille : {textile.taille}</h5>
                    <h5 className="card-text">Elasthanne : {textile.elasthanne === true ? 'Oui' : 'Non'}</h5>
                    <h5 className="card-text">Lavage : {textile.lavage}</h5>
                    <h5 className="card-text">Categorie: {cat_libelle}</h5>
                    <h5 className="card-text">Prix : {textile.prix} €</h5>
                </div>
                {
                    this.props.user !== null &&
                    <form encType="multipart/form-data" onSubmit={(e) => this.submitTextile(e,id,index,textileImage,libelle,marque,prix,taille,elasthanne,lavage)}>
                        <button type="submit" className="btn btn-success">Ajouter au panier</button>
                    </form>
                }
                {
                    this.props.user.role === 10 &&
                    <button type="onClick" className="btn btn-warning" onClick={this.togglebutton}>
                        Modifier
                    </button>
                }

            </div>


            <hr />
            {open === true ? <div className="card hidden" >
                <h2> Modification TEXTILE</h2>
                <form encType="multipart/form-data" onSubmit={(e) => this.submit(e)} >

                    <div className="row">
                        <div className="col-md-4">
                            <div className="form-group">
                                <label> Libelle </label>
                                <input type="text" id="libelle" className="form-control" value={libelle} required onChange={(e) => this.handleChange(e)} />
                            </div>
                        </div>

                        <div className="col-md-4">
                            <div className="form-group">
                                <label> Marque </label>
                                <input type="text" id="marque" className="form-control" value={marque} required onChange={(e) => this.handleChange(e)} />
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-2">
                            <div className="form-group">
                                <label> Prix </label>
                                <input type="number" id="prix" className="form-control" value={prix} required onChange={(e) => this.handleChange(e)} />
                            </div>
                        </div>

                        <div className="col-md-2">
                            <div className="form-group">
                                <label> Stock </label>
                                <input type="number" id="stock" className="form-control" value={stock} required onChange={(e) => this.handleChange(e)} />
                            </div>
                        </div>

                        <div className="col-md-2">
                            <div className="form-group">
                            <label> Categorie </label>
                                <select id="category"  required className="form-control" onChange={(e) => this.handleChange(e)}>
                                        {
                                            categories.map((category, toto) => {
                                                if(category._id == index){
                                                    return <option key={toto} selected value={category._id}> {category.libelle} </option>
                                                }
                                                else{
                                                } return <option key={toto} value={category._id}> {category.libelle} </option>
                                            })
                                        }
                                </select>
                            </div>
                        </div>

                        <div className="col-md-2">
                            <div className="form-group">
                                <label> Lavage </label>
                                <input type="string" id="lavage" className="form-control" value={lavage} required onChange={(e) => this.handleChange(e)} />
                            </div>
                        </div>

                        <div className="col-md-2">
                            <div className="form-group">
                                <label> Taille </label>
                                <input type="string" id="taille" className="form-control" value={taille} required onChange={(e) => this.handleChange(e)}>
                                </input>
                            </div>
                        </div>

                        <div className="col-md-2">
                            <div className="form-group">
                                <label> Elasthanne </label>
                                <select id="elasthanne" className="form-control" value={elasthanne} required onChange={(e) => this.handleChange(e)}>
                                    <option value={true}> Oui </option>
                                    <option value={false}> Non </option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <br></br>
                    <button type="submit" className="btn btn-success">
                        Valider
                    </button>
                </form>
            </div>
                : null
            }

            {open === true ? <div className="card hidden" >
                <h2> Modification Image</h2>
                <form encType="multipart/form-data" onSubmit={(e) => this.submitImage(e)} >



                    <div className="row">
                        <div className="col-md-4">
                            <div className="form-group">
                                <label> Image </label>
                                <input type="file" id="image" className="form-control" required onChange={(e) => this.handleChangeImage(e)} />
                            </div>
                        </div>
                    </div>

                    <br></br>
                    <button type="submit" className="btn btn-success">
                        Valider Image
                    </button>
                </form>
            </div>
                : null
            }

        </div>
    }
}

//acces au state général en pointant user
const mapStateToProps = state => {
    return { user: state.user }
};

export default connect(mapStateToProps, null)(TextileDetails);