import React, { Component } from 'react'
import Textile from '../../components/Textile';
import TextileService from '../../services/textile.service';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import TextileCommandeService from '../../services/commandeTextile.service';

class Textiles extends Component {
    constructor(props) {
        super(props);
        this.state = {
            textiles: [],
        }
        this.backButton = this.backButton.bind(this);
    }

    backButton() {
        try {
            this.props.history.goBack()
        } catch (e) {
            console.error(e);
        }
    }

    async componentDidMount() {

        try {
            let response = await TextileService.list();
            this.setState({ textiles: response.data.articles });
        } catch (e) {
            console.error(e);
        }
    }
    async submitTextile(e,id_article, category,article_image,libelle, marque, prix, taille, elasthanne, lavage) {
        e.preventDefault();
        let body = {
            libelle: libelle, 
            image: article_image, 
            marque: marque, 
            prix: prix, 
            taille: taille, 
            elasthanne: elasthanne, 
            lavage: lavage,
            id_article:id_article,
            status:"en cours",
            id_user:this.props.user._id,
            category:category
        }
        //console.log(body);
        try {
            await TextileCommandeService.add(body);
           
            this.props.history.push('/panier');
        } catch (e) {
            console.error(e);
        }
    }

    async deleteTextile(id) {
        try {
            await TextileService.delete(id);

            let response = await TextileService.list();
            this.setState({ textiles: response.data.articles });
            this.props.history.push('/articles/textile');

        } catch (e) {
            console.error(e);
        }
    }

    render() {
        let { textiles } = this.state;
        return <div className="container-fluid">

            <h1>TEXTILES</h1>
            <div className="btn-toolbar justify-content-between" role="toolbar" aria-label="Toolbar with button groups">
                <div className="btn-group" role="group" aria-label="First group">
                    <h2>Liste des textiles </h2>
                    {
                        this.props.user.role === 10 &&
                        <Link to={'/articles/textile/add'} className="btn btn-primary">Ajouter un textile</Link>
                    }
                </div>
                <div className="input-group">
                    <button type="onClick" className="btn btn-primary" onClick={this.backButton}>Retour</button>
                </div>
            </div>


            {
                this.props.user !== null &&
                <div className="row">
                    {
                        textiles.map((textile, index) => {
                            return <div className="card col-md-3 mb-4">

                                <Textile
                                    key={index}
                                    libelle={textile.libelle}
                                    marque={textile.marque}
                                    prix={textile.prix}
                                    stock={textile.stocke}
                                    image={`${process.env.REACT_APP_HOST_API}/${textile.image}`}
                                    taille={textile.taille}
                                    elasthanne={
                                        textile.elasthanne === true ? 'Oui' : 'Non'
                                    }
                                    lavage={textile.lavage}
                                    category={textile.category}
                                />

                                <Link to={`/articles/textile/info/${textile._id}`} className="btn btn-primary">Voir le produit</Link>
                                {
                                    this.props.user !== null &&
                                    <form encType="multipart/form-data" onSubmit={(e) => this.submitTextile(e,textile._id,textile.category,textile.image,textile.libelle,textile.marque,textile.prix,textile.taille,textile.elasthanne,textile.lavage)}>
                                        <button type="submit" className="btn btn-success">Ajouter au panier</button>
                                    </form>
                                }
                                {
                                    this.props.user.role === 10 &&
                                    <button className='btn btn-danger' onClick={() => this.deleteTextile(textile._id)}>
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-trash" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                            <path fillRule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                                        </svg>
                                    </button>
                                }
                            </div>
                        })
                    }
                </div>
            }
        </div>
    }
}

//acces au state général en pointant user
const mapStateToProps = state => {
    return { user: state.user }
};

export default connect(mapStateToProps, null)(Textiles);