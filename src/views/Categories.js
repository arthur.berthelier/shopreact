import React, { Component } from 'react'
import CategoryService from '../services/category.service'

export default class Categories extends Component {

    constructor(props) {
        super(props);
        this.state = {
            categories: [],
            category: null,
            libelle: null,
            updatedCategory: {id:"",libelle:""},
           // open: true
          
        }
        //this.togglebutton = this.togglebutton.bind(this);
    }

    async componentDidMount() {
        try {
            let response = await CategoryService.list();
            this.setState({ categories: response.data.categories });
        } catch (e) {
            console.error(e);
        }
    }

    handleChange(e) {
        this.setState({
            [e.target.id]: e.target.value
        });
    }


    handleChangeUpdate(e) {
        this.setState({
            updatedCategory: { ...this.state.updatedCategory, libelle: e.target.value }
        });
    }

    handleClickBtnModal(category) {
        this.setState({ updatedCategory: { id: category._id, libelle: category.libelle } });
    }
    /** 
    togglebutton() {
        const open  = this.state.open;  
        //console.log(open);
       this.setState({  
        open: !open,  
       });    
    }
    */
    async deleteCategory(id) {
        try {
            await CategoryService.delete(id);
            let response = await CategoryService.list();
            this.setState({ categories: response.data.categories });
            this.props.history.push('/categories');

        } catch (e) {
            console.error(e);
        }
    }

    async submit(e) {
        e.preventDefault();
        try {
            let body = { libelle: this.state.libelle };
            let response = await CategoryService.create(body);
            let newCat = response.data.category;
            let { categories } = this.state; 
            categories.push(newCat);
            this.setState({ categories, category: '' });
            this.libelle.value = ""; // clear du champ input 
        
        } catch (e) {
            console.error(e);
        }
    }

    //update fonctionne mais on la modal disparait pas + refresh manuellement pour voir les modifications
    async submitUpdate(e) {
        e.preventDefault();
        try {
            let body = { libelle: this.state.updatedCategory.libelle };
            let response = await CategoryService.update(this.state.updatedCategory.id, body);
            //On retrouve la valeur a modifier par l'id , dans le tableau stocker dans le state
            this.state.categories.map((category) => {
                if (category._id === response.data.category._id)
                {
                    category.libelle = response.data.category.libelle;
                }
            });
            
            this.setState({ categories:  this.state.categories}); // on update notre state avec le nouveau tableau 
        } catch (e) {
            console.error(e);
        }
        return false;
    }

    render() {
        let { categories ,open } = this.state;
        return <div className="container">
            <h1>CATEGORIES</h1>
            <h2> Liste des catégories </h2>

            <table className="table table-hover">
                <thead className="thead-dark">
                    <tr>
                        <th>Libelle</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        categories.map((category, index) => {
                            return <tr key={index}>
                                <td>{category.libelle}</td>
                                <td>
                                    <button type="button" className="btn btn-warning" data-toggle="modal" data-target="#modalUpdate"
                                        onClick={() => this.handleClickBtnModal(category)}>
                                        Modifier
                                    </button>
                                    <button className='btn btn-danger' onClick={() => this.deleteCategory(category._id)}>
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-trash" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                            <path fillRule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                                        </svg>
                                    </button>
                                </td>
                            </tr>
                        })
                    }
                </tbody>
            </table>

            <hr />


            <form onSubmit={(e) => this.submit(e)}>
                <div className="form-group">
                    <div className="modal" id="modalAdd" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title" id="exampleModalLabel">Ajout d'une Categorie</h5>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="modal-body">
                                    <div className="form-group">
                                    <label>Libelle</label>
                                        <input type="text" ref={(el) => (this.libelle = el)} id="libelle" className="form-control " required onChange={(e) => this.handleChange(e)} />
                                    </div>


                                </div>
                                <div className="modal-footer">
                                <button type="submit" className="btn btn-primary" data-toggle="modal" data-target="#modalAdd">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-plus" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fillRule="evenodd" d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
                                    </svg>
                                </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <button type="button" className="btn btn-success" data-toggle="modal" data-target="#modalAdd">
                    Ajouter
            </button>

             <form onSubmit={(e) => this.submitUpdate(e)}>
                <div className="modal" id="modalUpdate" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Modification de la Categorie</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div className="form-group">
                                    <label>Categorie</label>
                                    <input type="text" id="updatedCategory" className="form-control" value={this.state.updatedCategory.libelle}  required
                                        onChange={(e) => this.handleChangeUpdate(e)}></input>
                                </div>


                            </div>
                            <div className="modal-footer">
                                <button type="submit" data-toggle="modal" data-target="#modalUpdate" className="btn btn-primary">Enregistrer</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    }
}

