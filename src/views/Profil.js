import React, { Component } from 'react'
import UserService from '../services/user.service';
import { connect } from 'react-redux';
import { updateUser } from '../actions/users.actions';

class Profil extends Component {

    constructor(props) {
        super(props);
        this.state = {
            user:[],
            firstname: null,
            lastname: null,
            mail: null,
            password: null,
            phone: null, 
            address: null,
            zip_code: null,
            
        }
    }

    handleChange(e) {
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    async componentDidMount() {
        try {
            let { _id } = this.props.user;
            let response = await UserService.details(_id);
            let user =  response.data.user;
            this.setState({ 
                user: user, 
                firstname: user.firstname, 
                lastname: user.lastname, 
                mail: user.mail, 
                password: user.password, 
                phone: user.phone, 
                address: user.address, 
                zip_code: user.zip_code, 
            });
        } catch (e) {
            console.error(e);
        }
    }

    async submit(e) {
        e.preventDefault();
        try {
            
            let { _id } = this.props.user;
            let { firstname, lastname, mail, password,phone, address, zip_code } = this.state;
            
            let body = {
                firstname: firstname, 
                lastname: lastname, 
                mail:mail, 
                password: password, 
                phone: phone, 
                address: address, 
                zip_code: zip_code,}
            let response = await UserService.update(_id, body);
            
            this.setState({ 
                firstname: firstname,
                lastname: lastname,
                mail: mail,
                password: password,
                phone: phone, 
                address: address,
                zip_code: zip_code, });
            this.props.updateUser(response.data.user);
            this.props.history.push(`/`);
        } catch (e) {
            console.error(e);
        }
    }

    render() {
        let { firstname, lastname, mail, password,phone, address, zip_code} = this.state;
        return (
            <div className="container">
                <div className="card">
                    <div className="row">
                        <div className="col">
                            <div className="card-block px-4">
                                <div className="col-lg">
                                    <div className="p-4">
                                        <div className="text-center">
                                            <h1>Vos informations</h1>
                                        </div>

                                        <form onSubmit={(e) => this.submit(e)} >
                                        <div className="form-group">
                                            <label>Prénom</label>
                                            <input type="string" value={firstname || ''} required className="form-control" id="firstname"
                                                onChange={(e) => this.handleChange(e)} />
                                        </div>
                                        <div className="form-group">
                                            <label>Nom</label>
                                            <input type="string" value={lastname || ''} required className="form-control" id="lastname"
                                                onChange={(e) => this.handleChange(e)} />
                                        </div>
                                        <div className="form-group">
                                            <label>Email</label>
                                            <input type="mail" value={mail || ''}required className="form-control" id="mail"
                                                onChange={(e) => this.handleChange(e)} />
                                        </div>
                                        <div className="form-group">
                                            <label>Mot de passe</label>
                                            <input type="password" value={password || '' }  className="form-control" id="password"
                                                onChange={(e) => this.handleChange(e)} />
                                        </div>
                                        <div className="form-group">
                                            <label>Téléphone</label>
                                            <input type="number" value={phone || ''}className="form-control" id="phone"
                                                onChange={(e) => this.handleChange(e)} />
                                        </div>
                                        <div className="form-group">
                                            <label>Adresse</label>
                                            <input type="string" value={address || ''}className="form-control" id="address"
                                                onChange={(e) => this.handleChange(e)} />
                                        </div>
                                        <div className="form-group">
                                            <label>Code postal</label>
                                            <input type="string" value={zip_code || ''} required className="form-control" id="zip_code"
                                                onChange={(e) => this.handleChange(e)} />
                                        </div>
                                        <button type="submit" className='btn btn-primary'>Mettre a jour vos informations</button>
                                        <p>{this.props.name}</p>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-auto">
                            <img src="/img/logo_profil.png"></img>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

//UTILISATION DE REDUX POUR L'IDENTIFICATION
// LECTURE RETURN un object avec des attributs du state globale permet de renvoyer le user
const mapStateToProps = state => {
    return {user: state.user}
};

//ECRITURE RETURN un object avec des fonctiion utiliser pour modifier une valeur affecter une valeur 
const mapDispatchToProps = dispatch => {
    return {
        updateUser: user=> dispatch(updateUser(user))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Profil);