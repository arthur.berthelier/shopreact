import React, { Component } from 'react';
import { connect } from 'react-redux';
import SacService from '../services/sac.service';
import Sac from "../components/Sac";
import TextileService from '../services/textile.service';
import Textile from "../components/Textile";
import SacCommandeService from '../services/commandeSac.service';
import TextileCommandeService from '../services/commandeTextile.service';

class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            articles: [],
            textiles:[],
            libelle: null,
            marque: null,
            image: null,
            prix: null,
            stock: null,
            contenance: null,
            anse: true,
            refermable: true,
            category:null,
         
        }
    }

    async componentDidMount(){
        try {
            let response_sac = await SacService.list();
            let response_textile = await TextileService.list(); 
            this.setState({articles: response_sac.data.articles})
            this.setState({textiles: response_textile.data.articles})
        } catch (e) {
            
        }
    }
    async submitSac(e, id_article, category,article_image,libelle, marque, prix, contenance, anse, refermable) {
        e.preventDefault();

        let body = {libelle: libelle, image: article_image, marque: marque, prix: prix, contenance: contenance, anse: anse, refermable: refermable,id_article:id_article,status:"en cours",id_user:this.props.user._id,category:category}
        try {
            await SacCommandeService.add(body);
            this.props.history.push('/panier');
        } catch (e) {
            console.error(e);
        }
    }
    
    async submitCommandeSac(e, id_article, category,article_image,libelle, marque, prix, contenance, anse, refermable) {
        e.preventDefault();

        let body = {libelle: libelle, image: article_image, marque: marque, prix: prix, contenance: contenance, anse: anse, refermable: refermable,id_article:id_article,status:"terminer",id_user:this.props.user._id,category:category}
        try {
            await SacCommandeService.add(body);
            this.props.history.push('/commande');
        } catch (e) {
            console.error(e);
        }
    }

    async submitCommandeTextile(e,id_article, category,article_image,libelle, marque, prix, taille, elasthanne, lavage) {
        e.preventDefault();
        let body = {
            libelle: libelle, 
            image: article_image, 
            marque: marque, 
            prix: prix, 
            taille: taille, 
            elasthanne: elasthanne, 
            lavage: lavage,
            id_article:id_article,
            status:"terminer",
            id_user:this.props.user._id,
            category:category
        }
      
        try {
            await TextileCommandeService.add(body);
           
            this.props.history.push('/commande');
        } catch (e) {
            console.error(e);
        }
    }
    async submitTextile(e,id_article, category,article_image,libelle, marque, prix, taille, elasthanne, lavage) {
        e.preventDefault();
        let body = {
            libelle: libelle, 
            image: article_image, 
            marque: marque, 
            prix: prix, 
            taille: taille, 
            elasthanne: elasthanne, 
            lavage: lavage,
            id_article:id_article,
            status:"en cours",
            id_user:this.props.user._id,
            category:category
        }
        try {
            await TextileCommandeService.add(body);
           
            this.props.history.push('/panier');
        } catch (e) {
            console.error(e);
        }
    }

    render() {
        let { articles, textiles } = this.state;
        return <div className="container-fluid">

            <h1>SHOP Arthur et Lucie</h1>
            <div id="demo" className="carousel slide" data-ride="carousel">

                
                <ul className="carousel-indicators">
                    <li data-target="#demo" data-slide-to="0" className="active"></li>
                    <li data-target="#demo" data-slide-to="1"></li>
                    <li data-target="#demo" data-slide-to="2"></li>
                </ul>

                <div className="carousel-inner">
                    <div className="carousel-item active justify-content-center text-center">
                        <img src="/img/sac_promo.png" alt=""/>
                    </div>
                    <div className="carousel-item justify-content-center text-center">
                        <img src="/img/vetement_promo.png" alt=""/>
                    </div>
                    <div className="carousel-item justify-content-center text-center">
                        <img src="/img/livraison.jpg" alt=""/>
                    </div>
                </div>


                <a className="carousel-control-prev" href="#demo" data-slide="prev">
                    <span className="carousel-control-prev-icon"></span>
                </a>
                <a className="carousel-control-next" href="#demo" data-slide="next">
                    <span className="carousel-control-next-icon"></span>
                </a>

            </div>
            <p>
                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nam, incidunt accusamus culpa repellendus, vel delectus laborum harum fugiat consequuntur facilis quisquam minus placeat mollitia, ducimus eligendi dolorum quo voluptas modi? 
            </p>

            <h2> Nos Sacs</h2>
        
            <div className="row">
                {
                    articles.map((article, index_sac) => {
                        return <div className="card col-md-2 mb-3">
                            <Sac
                            key={index_sac}
                            libelle={article.libelle}
                            marque={article.marque}
                            prix={article.prix}
                            stock={article.stocke}
                            image={`${process.env.REACT_APP_HOST_API}/${article.image}`}
                            refermable={article.refermable === true ? 'Oui' : 'Non'}
                            anse={article.anse === true ? 'Oui' : 'Non'}
                            contenance={article.contenance}
                            category={article.category}
                        />
                        
                        {
                            this.props.user !== null &&
                            <form encType="multipart/form-data" onSubmit={(e) => this.submitSac(e,article._id,article.category,article.image,article.libelle,article.marque,article.prix,article.contenance,article.anse,article.refermable)}>
                                <button type="submit" className="btn btn-success">Ajouter au panier</button>
                            </form>
                        }
                        {
                            this.props.user !== null &&
                            <form encType="multipart/form-data" onSubmit={(e) => this.submitCommandeSac(e,article._id,article.category,article.image,article.libelle,article.marque,article.prix,article.contenance,article.anse,article.refermable)}>
                                <button type="submit" className="btn btn-success">Commander le produit !</button>
                            </form>
                        }
                       

                        </div>
                    })
                }
            </div> 

            <h2> Nos Textiles</h2>
        
            <div className="row">
                {
                    textiles.map((textile, index_textile) => {
                        return <div className="col-md-2 mb-3">
                            <Textile
                            key={index_textile}
                            libelle={textile.libelle}
                            marque={textile.marque}
                            prix={textile.prix}
                            stock={textile.stocke}
                            image={`${process.env.REACT_APP_HOST_API}/${textile.image}`}
                            taille={textile.taille}
                            elasthanne={textile.elasthanne === true ? 'Oui' : 'Non'}
                            lavage={textile.lavage}
                            category={textile.category}
                        />
                        {
                            this.props.user !== null &&
                            <form encType="multipart/form-data" onSubmit={(e) => this.submitTextile(e,textile._id,textile.category,textile.image,textile.libelle,textile.marque,textile.prix,textile.taille,textile.elasthanne,textile.lavage)}>
                                <button type="submit" className="btn btn-success">Ajouter au panier</button>
                            </form>
                        }
                         {
                            this.props.user !== null &&
                            <form encType="multipart/form-data" onSubmit={(e) => this.submitCommandeTextile(e,textile._id,textile.category,textile.image,textile.libelle,textile.marque,textile.prix,textile.taille,textile.elasthanne,textile.lavage)}>
                                <button type="submit" className="btn btn-success">Commander le produit !</button>
                            </form>
                        }
                        </div>
                    })
                }
            </div> 
        </div>
    }
}
//UTILISATION DE REDUX POUR L'IDENTIFICATION
// LECTURE RETURN un object avec des attributs du state globale permet de renvoyer le user
const mapStateToProps = state => {
    return {user: state.user}
};

export default connect(mapStateToProps, null)(Home);