import React, { Component } from 'react';
import SacCommandeService from '../services/commandeSac.service';
import Sac from "../components/Sac";
import TextileCommandeService from '../services/commandeTextile.service';
import Textile from "../components/Textile";
import { connect } from 'react-redux';

class Commande extends Component {

    constructor(props) {
        super(props);
        this.state = {
            articles: [],
            textiles:[]
        }
    }

    async componentDidMount(){
        try {
            let response_sac = await SacCommandeService.listCommande(this.props.user._id); 
            let response_textile = await TextileCommandeService.listCommande(this.props.user._id); 
            this.setState({articles: response_sac.data.articles})
            this.setState({textiles: response_textile.data.articles})
        } catch (e) {
            
        }
    }

   


    render() {
        let { articles, textiles } = this.state;
        return <div className="container-fluid">

            <h1>Vos commandes</h1>
            <h2>Vos Sacs</h2>
            <div className="row">
                {
                    articles.map((article, index_sac) => {
                        return <div className="col-md-2 mb-3">
                            <Sac
                            key={index_sac}
                            libelle={article.libelle}
                            marque={article.marque}
                            prix={article.prix}
                            stock={article.stocke}
                            image={`${process.env.REACT_APP_HOST_API}/${article.image}`}
                            refermable={article.refermable === true ? 'Oui' : 'Non'}
                            anse={article.anse === true ? 'Oui' : 'Non'}
                            contenance={article.contenance}
                            category={article.category}
                        />
                        </div>
                    })
                }
            </div> 

            <h2>Vos Textiles</h2>
        
            <div className="row">
                {
                    textiles.map((textile, index_textile) => {
                        return <div className="col-md-2 mb-3">
                            <Textile
                            key={index_textile}
                            libelle={textile.libelle}
                            marque={textile.marque}
                            prix={textile.prix}
                            stock={textile.stocke}
                            image={`${process.env.REACT_APP_HOST_API}/${textile.image}`}
                            taille={textile.taille}
                            elasthanne={textile.elasthanne === true ? 'Oui' : 'Non'}
                            lavage={textile.lavage}
                            category={textile.category}
                        />
                        </div>
                    })
                }
            </div> 
        </div>
    }
}
//UTILISATION DE REDUX POUR L'IDENTIFICATION
// LECTURE RETURN un object avec des attributs du state globale permet de renvoyer le user
const mapStateToProps = state => {
    return {user: state.user}
};

export default connect(mapStateToProps, null)(Commande);