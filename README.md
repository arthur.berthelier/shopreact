**#CONTEXT DU PROJET:**

Le Shop A&L est un site de e-shop en ligne fait en NODE.JS pour l'api et en REACT pour le front. Le but est de proposer à de futur client une série d'articles à acheter sur notre site.
Elle permet de s'inscrire pour créer un compte utilisateur (rôle “0”) ou de se connecter .
Les comptes administrateurs sont déjà créés dans la base de données avec un rôle “10”.

Les administrateurs peuvent :
- créer, lister, modifier et supprimer un article (sac ou textiles)
- créer, lister, modifier et supprimer une catégorie (relier à un article)

Les  utilisateurs peuvent :
- lister les articles(sac ou textiles)
- voir le détails d’un article(sac ou textiles)
- l’ajout au panier
- passer une commande (ce qui mettra son ou ses articles en statut terminer)
- Voir leur profil et le modifier

Nous avons utiliser de l'héritage pour les articles (sacs et textiles) et pour la commande (deux statut différents : en cours et terminer).
Pour les articles car en effet sacs et textiles sont tous deux des articles qui posséde les mêmes informations essentiels (libelle, marque, prix, stock,image, categorie) puis qui différe sur 3 autres informations.
Pour les commandes, il faut ajouter dans le panier les articles que le client voudrait acheter (statut en cours car la commande n'est pas finalisé) puis quand il aura payer ses articles (fictivement, il click en réaliser sur payer ou passer la commande) et alors les articles de cette tables pour cette user passeront en statut terminer.

**#PROBLÈMES RENCONTRÉS : **

UPDATE :L’update d’une image a été un point bloquant pendant plusieurs temps, décision de scinder en deux l’update d’un côté l'image et de l’autre les informations, après la résolutions du soucis de l’update de l’image, l’update du texte ne fonctionnait pas non plus donc blocage pendant plusieurs jour puis résolution, problème aussi sur la récupération/update de la catégorie

MODALE:Le refresh ne fonctionnait pas, blocage puis résolution

YAMMER: rédaction d’une manière beaucoup plus longue au début puis retour arrière pour le mettre en un seul fichier sur le back

PANIER/COMMANDE: plus complexe qu’il n’y paraît, à pris beaucoup de temps

LOGIN: L’utilisation de redux à bloquer les tests de fonctionnement 
